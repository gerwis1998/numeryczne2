from time import time

from matplotlib import pyplot

from Matrix import *
from Vector import *


def time_func(func, args):
    starting_time = time()
    print("Wynik: " + str(func(*args)))
    return time() - starting_time


def jacobi_method(a, b, e):
    matrix_a = copy_matrix(a)
    vector_b = copy_vector(b)
    k = 0
    temp_x = zeros(len(matrix_a))
    x = copy_vector(temp_x)
    while True:
        for i in range(len(matrix_a)):
            value = vector_b[i]
            for j in range(len(matrix_a)):
                if i != j:
                    value -= matrix_a[i][j] * x[j]
            value /= matrix_a[i][i]
            temp_x[i] = value
        x = copy_vector(temp_x)
        res = vector_sub_vector(dot_product(matrix_a, x), vector_b)
        norm_value = norm(res)
        if norm_value < e:
            return k
        k += 1


def gauss_seidel_method(a, b, e):
    k = 0
    matrix_a = copy_matrix(a)
    vector_b = copy_vector(b)
    vector_x = zeros(len(matrix_a[0]))
    while True:
        for i in range(len(matrix_a)):
            temp = vector_b[i]
            for j in range(len(matrix_a)):
                if i != j:
                    temp -= matrix_a[i][j] * vector_x[j]
            temp /= matrix_a[i][i]
            vector_x[i] = temp

        res = vector_sub_vector(dot_product(matrix_a, vector_x), vector_b)
        if norm(res) < e:
            return k
        k += 1


def lu_factorization(a, b):
    M = len(a)

    matrix_a = copy_matrix(a)
    matrix_l = diagonal_to_square_matrix(ones(M))
    matrix_u = matrix_zeros(M, M)

    vector_b = copy_vector(b)
    vector_x = ones(M)
    vector_y = zeros(M)

    # LUx = b
    for j in range(M):
        for i in range(j + 1):
            matrix_u[i][j] += matrix_a[i][j]
            for k in range(i):
                matrix_u[i][j] -= matrix_l[i][k] * matrix_u[k][j]

        for i in range(j + 1, M):
            for k in range(j):
                matrix_l[i][j] -= matrix_l[i][k] * matrix_u[k][j]
            matrix_l[i][j] += matrix_a[i][j]
            matrix_l[i][j] /= matrix_u[j][j]

    # Ly = b
    for i in range(M):
        value = vector_b[i]
        for j in range(i):
            value -= matrix_l[i][j] * vector_y[j]

        vector_y[i] = value / matrix_l[i][i]

    # Ux = y
    for i in range(M - 1, -1, -1):
        value = vector_y[i]
        for j in range(i + 1, M):
            value -= matrix_u[i][j] * vector_x[j]
        vector_x[i] = value / matrix_u[i][i]

    res = vector_sub_vector(dot_product(matrix_a, vector_x), vector_b)
    return norm(res)


class Main:
    myIndex = "174297"
    matrixSize = int("9" + myIndex[-2] + myIndex[-1])
    matrixSize = 9 * 9 * 7
    a1A = 5 + int(myIndex[3])
    f = int(myIndex[2])
    a1C = 3
    a2 = -1
    a3 = -1
    e = pow(10, -9)

    # A + B
    matrix = Matrix(matrixSize, a1A, f)
    matrix_a = matrix.create_matrix_a()
    vector_b = matrix.create_vector_b()
    time = time_func(jacobi_method, (matrix_a, vector_b, e))
    print("Jacobi czas: " + str(time))
    time = time_func(gauss_seidel_method, (matrix_a, vector_b, e))
    print("Gauss-Seidl czas: " + str(time))
    #
    # C - nie zbiegaja sie
    matrix_c = matrix.create_matrix_c()
    # equation.initC(f, a1C, a2, a3)
    # print("C:\nJacobi: " + str(jacobi_method(matrix_c, vector_b, e)) +
    #      "\nGauss-Seidl: " + str(gauss_seidel_method(matrix_c, vector_b, e)))

    # D
    print("Normza z residuum liczonego metoda LU: " + str(lu_factorization(matrix_c, vector_b)))

    # E
    testingSizes = [100, 500, 1000, 2000]
    jacobi_times = []
    gauss_seidel_times = []
    lu_times = []
    print("Metoda Jacobiego: ")
    for i in range(len(testingSizes)):
        matrix.matrixSize = testingSizes[i]
        matrix_a = matrix.create_matrix_a()
        vector_b = matrix.create_vector_b()
        time = time_func(jacobi_method, (matrix_a, vector_b, e))
        jacobi_times.append(time)
        print("N = " + str(testingSizes[i]) + " czas: " + str(time) + "s")
    print("\nMetoda Gaussa-Seidla")
    for i in range(len(testingSizes)):
        matrix.matrixSize = testingSizes[i]
        matrix_a = matrix.create_matrix_a()
        vector_b = matrix.create_vector_b()
        time = time_func(gauss_seidel_method, (matrix_a, vector_b, e))
        gauss_seidel_times.append(time)
        print("N = " + str(testingSizes[i]) + " czas: " + str(time) + "s")
    print("\nMetoda LU")
    for i in range(len(testingSizes)):
        matrix.matrixSize = testingSizes[i]
        matrix_a = matrix.create_matrix_a()
        vector_b = matrix.create_vector_b()
        time = time_func(lu_factorization, (matrix_a, vector_b))
        lu_times.append(time)
        print("N = " + str(testingSizes[i]) + " czas: " + str(time) + "s")

    pyplot.plot(testingSizes, jacobi_times, label="Jacobi", color="red")
    pyplot.plot(testingSizes, gauss_seidel_times, label="Gauss-Seidl", color="green")
    pyplot.plot(testingSizes, lu_times, label="LU", color="blue")
    pyplot.legend()
    pyplot.grid(True)
    pyplot.ylabel('Czas (s)')
    pyplot.xlabel('Liczba niewiadomych')
    pyplot.title('Zależność czasu wykonywania od liczby niewiadomych')
    pyplot.savefig('fig.png')
    pyplot.show()


