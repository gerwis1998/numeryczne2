from Matrix import Matrix, copy_matrix


def zeros(length):
    return [0 for _ in range(length)]


def ones(length):
    return [1 for _ in range(length)]


def diag(a):
    temp = []
    for i in range(len(a)):
        temp.append(a[i][i])
    return temp


def copy_vector(vector):
    copy = []
    for elem in vector:
        copy.append(elem)
    return copy


def vector_sub_vector(a, b):
    tmp = copy_vector(a)
    for i in range(len(tmp)):
        tmp[i] -= b[i]
    return tmp


def vector_add_vector(a, b):
    tmp = copy_vector(a)
    for i in range(len(tmp)):
        tmp[i] += b[i]
    return tmp


def norm(vector):
    tmp = 0
    for elem in vector:
        tmp += elem ** 2
    return tmp ** 0.5


def dot_product(a, b):
    copy_a = copy_matrix(a)
    copy_b = copy_vector(b)
    m = len(copy_a)
    n = len(copy_a[0])
    c = zeros(m)

    for i in range(m):
        for l in range(n):
            c[i] += copy_a[i][l] * copy_b[l]
    return c
