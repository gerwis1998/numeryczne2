from math import sin


def copy_matrix(matrix):
    copy = []
    for row in matrix:
        new_row = []
        for elem in row:
            new_row.append(elem)
        copy.append(new_row)
    return copy


def matrix_sub_matrix(a, b):
    temp = copy_matrix(a)
    for i in range(len(temp)):
        for j in range(len(temp[0])):
            temp[i][j] -= b[i][j]
    return temp


def matrix_add_matrix(a, b):
    temp = copy_matrix(a)
    for i in range(len(temp)):
        for j in range(len(temp[0])):
            temp[i][j] += b[i][j]
    return temp


def matrix_zeros(x, y):
    return [[0 for _ in range(x)] for _ in range(y)]


def diagonal_to_square_matrix(vector):
    temp = matrix_zeros(len(vector), len(vector))
    for i in range(len(vector)):
        temp[i][i] = vector[i]
    return temp


class Matrix:
    def __init__(self, matrixSize, e, f):
        self.matrixSize = matrixSize
        self.e = e
        self.f = f

    def create_matrix(self, a1, a2, a3):
        a = []
        for i in range(self.matrixSize):
            row = []
            for j in range(self.matrixSize):
                if i == j:
                    row.append(a1)
                elif i - 1 <= j <= i + 1:
                    row.append(int(a2))
                elif i - 2 <= j <= i + 2:
                    row.append(int(a3))
                else:
                    row.append(int(0))
            a.append(row)
        return a

    def create_matrix_a(self):
        return self.create_matrix(self.e, -1, -1)

    def create_vector_b(self):
        b = []
        for i in range(self.matrixSize):
            b.append(sin(i * (self.f + 1)))
        return b

    def create_matrix_c(self):
        return self.create_matrix(3, -1, -1)
